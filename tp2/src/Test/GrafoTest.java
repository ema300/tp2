package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import tp2.Grafo;

public class GrafoTest
{
	@Test(expected = IllegalArgumentException.class)
	public void primerVerticeNegativoTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(-1, 4,5);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void primerVerticeExcedidoTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(5, 4,9);
	}

	@Test(expected = IllegalArgumentException.class)
	public void segundoVerticeNegativoTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(3, -1,9);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void segundoVerticeExcedidoTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 5,8);
	}

	@Test(expected = IllegalArgumentException.class)
	public void verticesIgualesTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 2,10);
	}

	@Test
	public void aristaExistenteTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 4,3);
		assertTrue( grafo.existeArista(2, 4) );
	}

	@Test
	public void aristaRepetidaTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 4,7);
		
		grafo.agregarArista(2, 4,8);
		assertTrue( grafo.existeArista(2, 4) );
	}
	
	@Test
	public void aristaInvertidaTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3,9);
		assertTrue( grafo.existeArista(3, 2) );
	}
	
	@Test
	public void aristaInexistenteTest()
	{
		Grafo grafo = new Grafo(5);
		assertFalse( grafo.existeArista(2, 0) );
	}
}





