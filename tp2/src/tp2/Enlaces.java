package tp2;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Enlaces implements Comparable<Enlaces> {
	@Override
	public String toString() {
		return "Enlaces [primCoor=" + primCoor + ", segCoor=" + segCoor + ", distancia=" + distancia + "]";
	}


	// Atributos
	Coordinate primCoor;
	Coordinate segCoor;
	double distancia;

	public Enlaces(Coordinate num1, Coordinate num2, double distancia) {
		this.primCoor = num1;
		this.segCoor = num2;
		this.distancia = distancia;
	}

	public Coordinate getPrimCoor() {
		return primCoor;
	}

	public void setPrimCoor(Coordinate primCoor) {
		this.primCoor = primCoor;
	}

	public Coordinate getSegCoor() {
		return segCoor;
	}

	public void setSegCoor(Coordinate segCoor) {
		this.segCoor = segCoor;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Enlaces other = (Enlaces) obj;
		if (primCoor != other.primCoor)
			return false;
		if (segCoor != other.segCoor)
			return false;
		return true;
	}

	
	@Override
	public int compareTo(Enlaces e) {
		if (e.getDistancia()<this.getDistancia()) {
			return -1;
		} 
		 if (e.getDistancia()>this.getDistancia()) {
			return 1;
		}
		else {
			return 0;
		}
	}


}