package tp2;

import java.util.HashSet;
import java.util.Set;

public class Grafo {
	// Representamos el grafo por su matriz de adyacencia
	private double[][] Grafo;

	
	public Grafo(int vertices) {
		Grafo = new double[vertices][vertices];
	}
//////// Operaciones sobre aristas ////////////////////7777
	public void agregarArista(int i, int j, double valor) {
		verificarIndices(i, j);
		Grafo[i][j] = Grafo[j][i] = valor;
	}

	public void borrarArista(int i, int j) {
		verificarIndices(i, j);
		Grafo[i][j] = Grafo[j][i] = 0;
	}

	public boolean existeArista(int i, int j) {
		verificarIndices(i, j);
		boolean hayArista;
		if (Grafo[i][j] != 0)
			hayArista = true;
		     else
		      hayArista = false;
		return hayArista;
	}
	public double verValor(int i, int j) {
		verificarIndices(i, j);
		if (Grafo[i][j] != 0) {
			return Grafo[i][j];
		}
		return 0;
	
	}
/////// Vecinos de un vertice /////////////
	public Set<Integer> vecinos(int i) {
		verificarVertice(i);

		Set<Integer> ret = new HashSet<Integer>();
		for (int j = 0; j < tamano(); ++j)
			if (i != j && existeArista(i, j))
				ret.add(j);

		return ret;
	}
////// Cantidad de vertices  /////////////
	public int tamano() {
		return Grafo.length;
	}
////////// Lanzamiento de excepciones si los indices no son validos ////////////7
	private void verificarIndices(int i, int j) {
		verificarVertice(i);
		verificarVertice(j);

		if (i == j)
			throw new IllegalArgumentException("No existen aristas entre un vertice y si mismo! vertice = " + i);
	}

	private void verificarVertice(int i) {
		if (i < 0 || i >= tamano())
			throw new IllegalArgumentException("El vertice " + i + " no existe!");
	}
	
	public void imprimirMatrizDeGrafo() {
		for (int x=0; x < Grafo.length; x++) {
			  System.out.print("|");
			  for (int y=0; y < Grafo[x].length; y++) {
			    System.out.print (Grafo[x][y]);
			    if (y!=Grafo[x].length-1) System.out.print("\t");
			  }
			  System.out.println("|");
			}	
	}

}
