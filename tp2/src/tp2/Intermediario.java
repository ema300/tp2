package tp2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Intermediario {
	private ArrayList<Coordinate> listaDePuntosEnElMapa;
	private ArrayList<Double> arregloDeCoordenadas;
	private ArrayList<Enlaces> listaDeDistanciasDelAGM; // agregado 29/9 CL
	private ArrayList<Enlaces> listaParaClusters; // agregado 06/10 CL

	public Intermediario() {
		//// clases a interactuar con la interfas Mapa
		arregloDeCoordenadas = new ArrayList<Double>();
		listaDePuntosEnElMapa = new ArrayList<Coordinate>();
		listaDeDistanciasDelAGM = new ArrayList<Enlaces>(); // agregado 29/9 CL
		listaParaClusters = new ArrayList<Enlaces>(); // agregado 06/10 CL
	}

///////////////////interactuar//////////////////  

/////////////////////////Lectura de archivos /////////////////////7777
	public void guardaContenido(File archivo) throws FileNotFoundException, IOException {

		String cadena;
		StringBuilder c = new StringBuilder();

		FileReader f = new FileReader(archivo);
		BufferedReader b = new BufferedReader(f);

		while ((cadena = b.readLine()) != null) {
			c.append(cadena);
		}
		b.close();
		limpiarTodo();
		crearUnaListaDeCoordenadas(c);
		crearUnaListaDePuntos();
		ArmarAGM(ArmarMatriz());
	}

////////////// creacion de listas ////////////7
	private void crearUnaListaDeCoordenadas(StringBuilder texto) {
		// ir leyendo el archivo y separarlos por coordenadas
		// separar en terminos hasta que encontremos en signo"-"
		/// o lleguemos al final de la cadena
		String s = "";

		for (int i = 0; i < texto.length(); i++) {
			if (texto.charAt(i) != '-' && texto.charAt(i) != ' ') {
				s = s + texto.charAt(i);
			}
			if (s.length()!=0 &&texto.charAt(i) == '-' || texto.length() - 1 == i) {
				this.arregloDeCoordenadas.add(Double.valueOf('-' + s));
				s = "";

			}

		}

	}

	public boolean crearUnaListaDePuntos() {

		if (this.arregloDeCoordenadas.size() % 2 == 0) {

			// si la lista tiene para sacar dos coordenadas
			for (int i = 0; i < arregloDeCoordenadas.size(); i = i + 2) {
				Coordinate c = new Coordinate(arregloDeCoordenadas.get(i), arregloDeCoordenadas.get(i + 1));
				this.listaDePuntosEnElMapa.add(c);
			}
			return true;
		}
		return false;
		/// reportar un fallo si no son pares las coordenadas

	}

	private ArrayList<Enlaces> listaDeEnlaces(Grafo grafo) {
		ArrayList<Enlaces> listaDeEnlaces = new ArrayList<Enlaces>();
		for (int i = 0; i < grafo.tamano(); i++) {
			for (int j = i + 1; j < grafo.tamano(); j++) {
				if (grafo.existeArista(i, j)) {
					Enlaces t = new Enlaces(listaDePuntosEnElMapa.get(i), listaDePuntosEnElMapa.get(j),
							grafo.verValor(i, j));
					listaDeEnlaces.add(t);
				}
			}
		}
		return listaDeEnlaces;
	}

	////////////////////// dar informacion al Exterior ///////////
	public ArrayList<Coordinate> coordenadas() {
		return listaDePuntosEnElMapa;

	}

	public ArrayList<Enlaces> ArbolGeneradorMinimo() {
		return listaDeDistanciasDelAGM;

	}

	public ArrayList<Enlaces> listaParaClusters() {
		return listaParaClusters;

	}

///////////////////////creacion de tablas //////////////////

	private Grafo ArmarMatriz() {
		int cantidadPuntos = listaDePuntosEnElMapa.size();
		double distancia;
		Grafo grafoCompleto = new Grafo(cantidadPuntos);
		for (int i = 0; i < cantidadPuntos; i++) {
			for (int j = i + 1; j < cantidadPuntos; j++) {
				distancia = distanciaDeEuclides(listaDePuntosEnElMapa.get(i), listaDePuntosEnElMapa.get(j));
				grafoCompleto.agregarArista(i, j, distancia);
			}
		}
		return grafoCompleto;

	}

	private AGM ArmarAGM(Grafo matriz) {
		AGM agm = new AGM(matriz);
		agm.construirArbol();
		ArmarListaDeDistanciaDelAGM(agm.dameGrafoAGM());

		return agm;

	}

	private void ArmarListaDeDistanciaDelAGM(Grafo grafo) { // agregado 29/9 CL
		listaDeDistanciasDelAGM = listaDeEnlaces(grafo);
		OrdenarListaDeDistanciaDelAGM();
	}

	private void OrdenarListaDeDistanciaDelAGM() {
		Collections.sort(listaDeDistanciasDelAGM);

	}

	public void GenerarCluster(int CantidadClusters) {
		limpiarListaParaCluster();

		int clusters = CantidadClusters - 1;

		ArmarListaParaClusters();
		int i = 0;
		while (i < clusters) {
			listaParaClusters.remove(0);
			i++;
		}

	}

	public void ArmarListaParaClusters() {
		for (Enlaces e : listaDeDistanciasDelAGM) {
			Enlaces enlace = new Enlaces(null, null, 0);
			enlace = e;
			listaParaClusters.add(enlace);
		}
	}

	public void limpiarListaParaCluster() {
		listaParaClusters.clear();
	}

	private void limpiarTodo() {
		listaDeDistanciasDelAGM.clear();
		arregloDeCoordenadas.clear();
		listaDePuntosEnElMapa.clear();
		listaParaClusters.clear();
	}

///////////////////// Calculo de Distancia (Euclides)////////////////////////

	private double distanciaDeEuclides(Coordinate PuntoUno, Coordinate PuntoDos) {
		// RAIZ( (x2-x1)^2 + (y2-y1)^2 )
		BigDecimal x1 = new BigDecimal(PuntoUno.getLat());
		BigDecimal y1 = new BigDecimal(PuntoUno.getLon());
		BigDecimal x2 = new BigDecimal(PuntoDos.getLat());
		BigDecimal y2 = new BigDecimal(PuntoDos.getLon());
		BigDecimal exponenciar1 = new BigDecimal("0.0");
		BigDecimal exponenciar2 = new BigDecimal("0.0");
		BigDecimal resta1 = new BigDecimal("0.0");
		BigDecimal resta2 = new BigDecimal("0.0");
		BigDecimal sumar = new BigDecimal("0.0");
		double resultado = 0;

		resta1 = x2.subtract(x1);
		resta2 = y2.subtract(y1);
		exponenciar1 = resta1.multiply(resta1);
		exponenciar2 = resta2.multiply(resta2);
		sumar = exponenciar1.add(exponenciar2);
		resultado = Math.sqrt(sumar.doubleValue());

		return resultado;

	}

}
