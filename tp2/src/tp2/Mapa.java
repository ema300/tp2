package tp2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.DefaultMapController;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;

import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;

public class Mapa {
	private Intermediario intermediario;
	private JFrame frame;
	private JTextField cantidadClusters;
	private JMapViewer mapa;
	private boolean flagTxt, flagPuntos, flagAgm;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mapa window = new Mapa();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Mapa() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		flagTxt = false;
		flagPuntos = false;
		flagAgm = false;
		intermediario = new Intermediario();
		mapa = new JMapViewer();
		frame = new JFrame();
		frame.getContentPane().setEnabled(false);
		frame.getContentPane().setFocusable(false);
		frame.getContentPane().setFocusTraversalKeysEnabled(false);
		frame.getContentPane().setBackground(new Color(32, 178, 170));
		frame.getContentPane().setForeground(new Color(102, 205, 170));
		frame.setBounds(100, 100, 755, 501);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(43, 11, 417, 410);
		panel.add(mapa);
		frame.getContentPane().add(panel);

		mapa.repaint();
		DefaultMapController mapController = new DefaultMapController(mapa);
		mapa.setLayout(null);
		mapController.setMovementMouseButton(MouseEvent.BUTTON1);
		
		////////////////// Abrir Archivo ////////////////////////////
		JButton btnNewButton = new JButton("Abrir Archivo");
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBackground(new Color(178, 34, 34));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				FileNameExtensionFilter filtro = new FileNameExtensionFilter(" *txt", "txt");
				chooser.setFileFilter(filtro);
				chooser.setApproveButtonText("Abrir .txt");
				chooser.addChoosableFileFilter(new FileFilter() {

					@Override
					public String getDescription() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public boolean accept(File f) {
						return false;
					}
				});

				int result = chooser.showOpenDialog(null);
				if (result != JFileChooser.CANCEL_OPTION) {
					File archivo = chooser.getSelectedFile();
					flagTxt = true;
					try {
						intermediario.guardaContenido(archivo);
						if (intermediario.crearUnaListaDePuntos()==false) {
							JOptionPane.showMessageDialog(null,"falta coordanada");

						}

					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			}
		});

		btnNewButton.setBounds(502, 36, 181, 43);
		frame.getContentPane().add(btnNewButton);
		///////////////// Mostrar Coordenadas ////////////////////////7
		JButton btnNewButton_1 = new JButton("Mostrar Coordenadas");
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.setBackground(new Color(0, 128, 128));
		btnNewButton_1.setBounds(502, 102, 181, 43);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (flagTxt == true) {
					flagPuntos = true;
					mapa.setDisplayPosition(intermediario.coordenadas().get(0), 13);
					dibujarPuntosEnElMapa(intermediario.coordenadas());
				}
			}
		});
		frame.getContentPane().add(btnNewButton_1);
		////////////////////////// AGM ////////////////////////////
		JButton btnNewButton_2 = new JButton("AGM");
		btnNewButton_2.setForeground(Color.WHITE);
		btnNewButton_2.setBackground(new Color(0, 128, 128));
		btnNewButton_2.setBounds(502, 171, 181, 43);

		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (flagPuntos == true) {
					flagAgm = true;
					dibujarArbolGeneradorMinimo();
				}
			}
		});

		frame.getContentPane().add(btnNewButton_2);

		/////////////////// Mostrar Cluster ////////////////////7
		JButton btnNewButton_3 = new JButton("Mostrar Clusters");
		btnNewButton_3.setForeground(Color.WHITE);
		btnNewButton_3.setBackground(new Color(0, 128, 128));
		btnNewButton_3.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				if (flagAgm == true && !cantidadClusters.getText().isEmpty()) {

					dibujarCluster();
				}

			}

		});

		btnNewButton_3.setBounds(504, 283, 179, 43);
		frame.getContentPane().add(btnNewButton_3);

		JLabel lblCluster = new JLabel("Clusters");
		lblCluster.setForeground(Color.WHITE);
		lblCluster.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCluster.setBounds(557, 253, 59, 14);
		frame.getContentPane().add(lblCluster);
		cantidadClusters = new JTextField();
		cantidadClusters.setBounds(504, 252, 46, 20);
		frame.getContentPane().add(cantidadClusters);
		cantidadClusters.setColumns(5);

		JLayeredPane layeredPane = new JLayeredPane();
		layeredPane.setBounds(502, 32, 21, -20);
		frame.getContentPane().add(layeredPane);

	}

	private void dibujarCluster() {
		mapa.setDisplayPosition(intermediario.coordenadas().get(0), 13);
		mapa.removeAllMapPolygons();
		String ingreso = cantidadClusters.getText();
		if (esNumero(ingreso)) {
			int clusters = Integer.parseInt(ingreso);

			if (clusters > 0 && clusters <= intermediario.ArbolGeneradorMinimo().size()) {
				intermediario.GenerarCluster(clusters);
				dibujarLineasEnElMapa(intermediario.listaParaClusters());
			} else {
				JOptionPane.showMessageDialog(null,
						"Debe ser mayor a 0 y menor que " + (intermediario.ArbolGeneradorMinimo().size()+1));

			}
		} else {
			JOptionPane.showMessageDialog(null, "Debe ser un n�mero");

		}

	}

	private void dibujarArbolGeneradorMinimo() {

		mapa.setDisplayPosition(intermediario.coordenadas().get(0), 13);
		mapa.removeAllMapPolygons();
		dibujarLineasEnElMapa(intermediario.ArbolGeneradorMinimo());
	}

	private void dibujarLineasEnElMapa(ArrayList<Enlaces> lista) {
		for (int i = 0; i < lista.size(); i++) {
			Coordinate A = new Coordinate(0, 0);
			Coordinate B = new Coordinate(0, 0);
			Enlaces enlace = new Enlaces(null, null, 0);
			enlace = lista.get(i);
			A = enlace.getPrimCoor();
			B = enlace.getSegCoor();
			mapa.addMapPolygon(new MapPolygonImpl(A, B, A));
		}
	}

	private void dibujarPuntosEnElMapa(ArrayList<Coordinate> lista) {
		for (Integer i = 0; i < lista.size(); i++) {
			Coordinate c = new Coordinate(0, 0);
			c = lista.get(i);
			mapa.addMapMarker(new MapMarkerDot(String.valueOf(i), c));

		}
	}

	private boolean esNumero(String i) {
		try {
			Integer.parseInt(i);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
